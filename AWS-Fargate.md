# Faregate Wordpress setup


## Navigation
- Login to AWS Console
- Search ECS and click on it

## Creating Wordpress app
- Click Clusters (left site panel) >> `Create Cluster`
- Select
	- Networking only >> next
	- cluster name - wordpress-jmeno
	- networking
		- tick `create VPC`
		- leave all to defaults (creates 2 public subnets)
		- click create
- Click on Task Definitions (left site panel)
	- click on create new task definition 
	- select `FARGATE` >> next step
	- task definition name - wordpress-task
	- task role - choose already existing one
	- network mode - awsvpc
	- task execution IAM role - choose existing one
	- task size
		- memory - 1GB
		- CPU - 0.5 vCPU
	- Add container:
		- name - mariadb
		- image - bitnami/mariadb:latest
		- port mapping - 3306 TCP
		- Advanced container configuration
			- enviromental variables:
				- ALLOW_EMPTY_PASSWORD=yes (value)
				- MARIADB_USER=bn_wordpress (value)
				- MARIADB_DATABASE=bitnami_wordpress (value)
		- all the rest leave blank/default
		- click `Add`
	- Add another container:
		- name - `wordpress`
		- image - `bitnami/wordpress`
		- port mapping - 80 TCP
		- Advanced container configuration
			- enviromental variables:
				- MARIADB_HOST=127.0.0.1 (value)
				- MARIADB_PORT_NUMBER=3306 (value)
				- WORDPRESS_DATABASE_USER=bn_wordpress (value)
				- WORDPRESS_DATABASE_NAME=bitnami_wordpress (value)
				- ALLOW_EMPTY_PASSWORD=yes (value)
		- all the rest leave blank/default
		- click `Add`
	- all the rest leave blank/default
	- click `Create`
- Click on newly created task
	- tick next to the task to select it
	- above click on `Actions` and select `create service`
		- launch type - FARGATE
		- service name - wordpress-service
		- service type - REPLICA
		- number of tasks - 1
		- all the rest leave blank/default
		- click next step
	- configure network
		- cluster VPC - choose your created VPC (10.x.x.x)
		- choose both subnets
		- Loadbalancer - none
		- service discovery name - wordpress-service
		- click next step
	- set autoscaling
		- service autoscaling - do not adjust the service's desired count
	- review
		- click `create service`

## Overlooking the result

- click on `clusters` on left side panel
- click on your cluster
- click on `Tasks` in the center panel
- click on task itself (long hash-like number e.g. 06a7cb07-b131-4e5d-b494-987d8b6f35a1)
- in the network section, copy the `Public IP`
- put it into browser and hit enter
- you should see wordpress welcome site

## Troubleshooting
- click on `clusters` on left side panel
- click on your cluster
- click on `Tasks` in the center panel
- click on task itself (long hash-like number e.g. 06a7cb07-b131-4e5d-b494-987d8b6f35a1)
- click on `Logs` in upper pannel (right bellow `Task: <id-of-your-task>`)
- select one container at a time and you will see the container/app logs
- you can do the same if you navigate to AWS CloudWatch service in AWS and select fargate logs


## Benefits
- Automatically created:
	- VPC with all networking necessary
	- Security groups + rules
	- NACLs + rules
	- Serverless container environment/cluster
	- Public IP
	- internal task networking
- Simple click-through setup
- Serverless infrastructure
- Ability to source container images from private AWS ECR image repository
- Mitigates hassling with Dockerfiles

## Cons
- By default all is put into Public Subnets rather then Private
- To get Production ready requires an extra effort
- Extensive app and routing logic may be tricky to setup

