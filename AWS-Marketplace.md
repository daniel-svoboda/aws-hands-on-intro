# Marketplace Wordpress setup

## Navigation
- Login to AWS Console
- Search for `Marketplace` and enter it

## Market place - Wordpress
- Search for Wordpress 
- Options are many, we recommend Wordpress from (link)[https://aws.amazon.com/marketplace/pp/B00NN8Y43U?ref_=beagle]

## Setup
- Choose the instance type
- Click on `Continue to Subscribe` and follow instructions

## Benefits
- All setup is completed for us
- We manage and own all underlying infrastructure

# Cons
- We pay a little premium for the comfort of using this automated template
- The premium may differ, for this Wordpress use is aprox. $0.002/hr = $1.5/month
