# Aws Hands On Intro

4 different ways to deploy Wordpress - lets practically show how wide spectrum of deployment methods AWS offers. This showcases 4 basic methods step-by-step:
1) AWS EC2 Virtual Machine
2) AWS Fargate
3) AWS Lightsail
4) AWS Marketplace

# Project introduction


This project has been setup to show several options how in AWS to get an application quickly up n running with and without the necessary boilerplate.

## Prerequisites

### SSH Keys

To interact with EC2 instances in AWS you will need to setup SSH Keys, create it via AWS console and assign them to every EC2 instance you will be using.

Registration of SSH keys:
- EC2: `AWS Console >> EC2 >> Network & Security >> Key Pairs >> Create Key Pair`, download it & keep safe
- Lightsail: you can create your own and upload it, or you can download & use the default one created for you by lightsail

### AWS Console

- Login to [Console](https://gem-system.signin.aws.amazon.com/console "console")
- You will receive temporary personal login details from your instructor

## Steps

### EC2

Follow instructions on [EC2](./AWS-EC2.md "EC2")

### Fargate

Follow instructions on [fargate](./AWS-Fargate.md "fargate")

### Lightsail

Follow instructions on [lightsail](./AWS-Lightsail.md "lightsail")

### Marketplace

Follow instructions on [marketplace](./AWS-Marketplace.md "marketplace")
