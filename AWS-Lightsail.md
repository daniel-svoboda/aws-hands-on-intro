# Lightsail Wordpress setup

## Navigation
- Login to AWS Console
- Search lightsail and click on it

## Creating Wordpress app
- Click `create instance`
- Select 
	- `Linux/unix` platform
	- Apps + OS and `WordPress`
	- Ssh key either create your own or use & download the default key
	- Instance type select according to app need, in example we will use the $3.5 small instance
	- Identify your instance with name of your choice
	- Tag your resources if requested
- Click create

## Overlooking the result
- Look at the lightsail web and browse through whats been automatically set for you
- Open the running app on the given IP address to ensure your app is running well

## Benefits
- What has been done for you:
	- Public IP auto-assigned
	- All Virtual Server spin up including installation of application
	- Ability to SSH & manage instance directly via lightsail console
	- Storage handling and ability to add storage on the go
	- Full networking setup
	- Monitoring/Logging setup
	- Snapshots and Backups

## Cons
- Very limited management over underlying resources
